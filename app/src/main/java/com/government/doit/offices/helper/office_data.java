package com.government.doit.offices.helper;

public class office_data {
	private String district;
	private String name;
	private String address;
	private String phone;
	private String fax;
	private String file;
    private String map;

	public String getDistrict() {return district;}
	public void setDistrict(String district) {
		this.district = district;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

}