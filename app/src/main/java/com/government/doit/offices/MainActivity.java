package com.government.doit.offices;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.government.doit.offices.MainClass.BirthCertificate;
import com.government.doit.offices.MainClass.Citizenship;
import com.government.doit.offices.MainClass.DrivingLicense;
import com.government.doit.offices.MainClass.Passport;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    public int currentimageindex = 0;
    ImageView slidingimage;
    Dialog dialog;
    private int[] IMAGE_IDS = {
            R.drawable.splash1, R.drawable.splash2, R.drawable.splash3, R.drawable.splash4
    };

    private ShareActionProvider mShareActionProvider;
    private Button btnNpLang, btnEnLang;

    public static final String OFFICEPREFERENCES = "prefs";
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor edit;
    public static final String strLangType = "LangType";

    String langType;
    TextView txtCitizen, txtPassport, txtBirth, txtDriving, txtElectic, txtCourt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        sharedpreferences = getSharedPreferences(OFFICEPREFERENCES, Context.MODE_PRIVATE);
        edit = sharedpreferences.edit();
        if (sharedpreferences.contains(strLangType))
            langType = sharedpreferences.getString(strLangType, "1");
        else
            langType = "1";

        txtCitizen = (TextView) findViewById(R.id.txtCitizen);
        txtPassport = (TextView) findViewById(R.id.txtPassport);
        txtBirth = (TextView) findViewById(R.id.txtBirth);
        txtDriving = (TextView) findViewById(R.id.txtDriving);
        txtElectic = (TextView) findViewById(R.id.txtElectic);
        txtCourt = (TextView) findViewById(R.id.txtCourt);

        if (langType.equalsIgnoreCase("1")) {
            txtCitizen.setText("नागरिकता");
            txtPassport.setText("राहदानी");
            txtBirth.setText("जन्म दर्ता");
            txtDriving.setText("चालक प्रमाणपत्र");
            txtElectic.setText("बिधुत प्राधिकरण");
            txtCourt.setText("अदालत");
        } else if (langType.equalsIgnoreCase("2")) {
            txtCitizen.setText(getString(R.string.strDrivingEn));
            txtPassport.setText(getString(R.string.strPassportEn));
            txtBirth.setText(getString(R.string.strBirthEn));
            txtDriving.setText(getString(R.string.strDrivingEn));
            txtElectic.setText(getString(R.string.strElecticEn));
            txtCourt.setText(getString(R.string.strCourtEn));
            getSupportActionBar().setTitle(getString(R.string.app_name));
        }

        final Handler mHandler = new Handler();
        final Runnable mUpdateResults = new Runnable() {
            public void run() {
                AnimateandSlideShow();
            }
        };
        int delay = 1000;
        int period = 3000;
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                mHandler.post(mUpdateResults);
            }
        }, delay, period);

        dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.select_language);

        btnNpLang = (Button) dialog.findViewById(R.id.nepali);
        btnEnLang = (Button) dialog.findViewById(R.id.english);

    }

    public void onClickLang(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        switch (v.getId()) {
            case R.id.nepali:
                edit.putString(strLangType, "1");
                edit.apply();
                finish();
                break;
            case R.id.english:
                edit.putString(strLangType, "2");
                edit.apply();
                finish();
                break;
        }
        startActivity(intent);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCitizen:
                Intent intent1 = new Intent(this, Citizenship.class);
                startActivity(intent1);
                break;
            case R.id.btnPassport:
                Intent intent2 = new Intent(this, Passport.class);
                startActivity(intent2);
                break;
            case R.id.btnBirth:
                Intent intent4 = new Intent(this, BirthCertificate.class);
                startActivity(intent4);
                break;
            case R.id.btnDriving:
                Intent intent5 = new Intent(this, DrivingLicense.class);
                startActivity(intent5);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem item = menu.findItem(R.id.menu_item_share);

        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
        setShareIntent(createShareIntent());

        return true;
    }

    private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null)
            mShareActionProvider.setShareIntent(shareIntent);

    }

    private Intent createShareIntent() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT,
                "http://stackandroid.com");
        return shareIntent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_item_share) {
            setShareIntent(createShareIntent());
            return true;
        }
        if (id == R.id.action_language) {
            dialog.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_facebook) {
            Intent i = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/DepartmentofIT/?fref=ts"));
            startActivity(i);
        } else if (id == R.id.nav_twitter) {
            Intent i = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://twitter.com/departmentofi"));
            startActivity(i);
        } else if (id == R.id.nav_google) {
            Intent i = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://plus.google.com/u/0/114693812677759747745/posts"));
            startActivity(i);
        } else if (id == R.id.nav_website) {
            Intent i = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://doit.gov.np/"));
            startActivity(i);
        }  else if (id == R.id.nav_calender) {
            Intent intent3 = new Intent(this, com.government.doit.offices.Calender.class);
            startActivity(intent3);
        }  else if (id == R.id.nav_location) {
            Intent intent3 = new Intent(this, ContactUs.class);
            startActivity(intent3);
        } else if (id == R.id.nav_feedback) {
            Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            emailIntent.setType("vnd.android.cursor.item/email");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"info@doit.gov.np"});

            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "About Office Finding App");
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
            startActivity(Intent.createChooser(emailIntent, "Send mail using..."));

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void AnimateandSlideShow() {
        slidingimage = (ImageView) findViewById(R.id.imageTitle);
        slidingimage.setImageResource(IMAGE_IDS[currentimageindex % IMAGE_IDS.length]);
        currentimageindex++;
        Animation rotateimage = AnimationUtils.loadAnimation(this, R.anim.custom_anim);


    }


}
