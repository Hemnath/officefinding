package com.government.doit.offices.helper;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.government.doit.offices.MainClass.BirthCertificate;
import com.government.doit.offices.MainClass.Citizenship;
import com.government.doit.offices.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by hemnath on 9/27/2016.
 */

public class BirthCirtiExpAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private HashMap<DistrictItem, List<OfficesItem>> officeItems = new HashMap<DistrictItem, List<OfficesItem>>();
    private HashMap<DistrictItem, List<OfficesItem>> officeSearchItems = new HashMap<DistrictItem, List<OfficesItem>>();
    private List<DistrictItem> groupItems = new ArrayList<DistrictItem>();
    private List<DistrictItem> groupSearch = new ArrayList<DistrictItem>();

    String langType;

    public BirthCirtiExpAdapter(Context context) {
        this.mContext = context;
        langType = ((BirthCertificate) mContext).getLangType();
    }

    public void setExpandableList(String district_name_en, String district_name_np, int district_id, String name_en, String name_np, String address_en, String address_np, String phone_en, String phone_np, String fax_en, String fax_np, String file, String image, String map_url) {
        boolean isAdded = false;

        for (int i = 0; i < groupItems.size(); i++) {
            DistrictItem item = groupItems.get(i);

            if (item.district_name_en.equalsIgnoreCase(district_name_en)) {

                List<OfficesItem> childItemList = officeItems.get(groupItems.get(i));
                List<OfficesItem> childItemList2 = officeSearchItems.get(groupSearch.get(i));

                OfficesItem OfficesItem = new OfficesItem(name_en, name_np, address_en, address_np, phone_en, phone_np, fax_en, fax_np, district_name_en, district_name_np, file, image, map_url, Integer.toString(district_id));
                childItemList.add(OfficesItem);
                officeItems.put(groupItems.get(i), childItemList);
                officeSearchItems.put(groupSearch.get(i), childItemList2);
                isAdded = true;
            }
        }

        if (!isAdded) {

            DistrictItem dItem = new DistrictItem(district_name_en, district_name_np, district_id);
            groupItems.add(dItem);
            groupSearch.add(dItem);
            List<OfficesItem> childItemList = new ArrayList<OfficesItem>();

            OfficesItem OfficesItem = new OfficesItem(name_en, name_np, address_en, address_np, phone_en, phone_np, fax_en, fax_np, district_name_en, district_name_np, file, image, map_url, Integer.toString(district_id));
            childItemList.add(OfficesItem);
            officeItems.put(dItem, childItemList);
            officeSearchItems.put(dItem, childItemList);
        }
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        clearItem();
        if (charText.length() == 0) {
            addAll();
        } else {
            for (DistrictItem wp : groupSearch) {
                if (wp.district_name_en.toLowerCase(Locale.getDefault()).contains(charText) || wp.district_name_np.contains(charText)) {
                    groupItems.add(wp);
                } else {
                    List<OfficesItem> childItemList = officeItems.get(wp);
                    List<OfficesItem> childItemList2 = officeSearchItems.get(wp);
                    List<OfficesItem> childNewItemList = new ArrayList<OfficesItem>();
                    for (OfficesItem ci : childItemList2) {
                        if (ci.name_en.toLowerCase(Locale.getDefault()).contains(charText) || ci.name_np.contains(charText)) {
                            childNewItemList.add(ci);
                        }
                    }
                    if (childNewItemList.size() > 0) {
                        childItemList.clear();
                        childItemList.addAll(childNewItemList);
                        groupItems.add(wp);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    public boolean clearItem() {
        groupItems.clear();
        return true;
    }

    public void addAll() {
        groupItems.clear();
        for (DistrictItem wp : groupSearch) {
            List<OfficesItem> childItemList = officeItems.get(wp);
            List<OfficesItem> childItemList2 = officeSearchItems.get(wp);
            List<OfficesItem> childNewItemList = new ArrayList<OfficesItem>();
            for (OfficesItem ci : childItemList2) {
                childNewItemList.add(ci);
            }
            if (childNewItemList.size() > 0) {
                childItemList.clear();
                childItemList.addAll(childNewItemList);
            }
        }
        groupItems.addAll(groupSearch);
    }

    public String OfficesId(int groupPosition, int childPosition) {
        OfficesItem item = (OfficesItem) getChild(groupPosition, childPosition);
        return item.Id;
    }

    public String OfficesName(int groupPosition, int childPosition) {
        String name = "";
        OfficesItem item = (OfficesItem) getChild(groupPosition, childPosition);
        if (langType.equalsIgnoreCase("1")) {
            name = item.name_np;
        } else if (langType.equalsIgnoreCase("2")) {
            name = item.name_en;
        }
        return name;
    }

    @Override
    public int getGroupCount() {
        return groupItems.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return officeItems.get(groupItems.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.groupItems.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.officeItems.get(this.groupItems.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView lblListHeader;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.office_list_group, parent, false);
            v.setTag(R.id.lblListOffice, v.findViewById(R.id.lblListOffice));
        }
        lblListHeader = (TextView) v.getTag(R.id.lblListOffice);

        DistrictItem item = (DistrictItem) getGroup(groupPosition);
        if (langType.equalsIgnoreCase("1"))
            lblListHeader.setText(item.district_name_np);
        else if (langType.equalsIgnoreCase("2"))
            lblListHeader.setText(item.district_name_en);

        return v;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView office_title, office_address, office_phone, office_fax;
        Button btnFile, btnMap;
        ImageView imageView;
        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.office_list_child, parent, false);
            v.setTag(R.id.office_title, v.findViewById(R.id.office_title));
            v.setTag(R.id.office_address, v.findViewById(R.id.office_address));
            v.setTag(R.id.office_phone, v.findViewById(R.id.office_phone));
            v.setTag(R.id.office_fax, v.findViewById(R.id.office_fax));
            v.setTag(R.id.office_image, v.findViewById(R.id.office_image));
            v.setTag(R.id.btnFile, v.findViewById(R.id.btnFile));
            v.setTag(R.id.btnMap, v.findViewById(R.id.btnMap));
        }
        office_title = (TextView) v.getTag(R.id.office_title);
        office_address = (TextView) v.getTag(R.id.office_address);
        office_phone = (TextView) v.getTag(R.id.office_phone);
        office_fax = (TextView) v.getTag(R.id.office_fax);
        imageView = (ImageView) v.getTag(R.id.office_image);
        btnFile = (Button) v.getTag(R.id.btnFile);
        btnMap = (Button) v.getTag(R.id.btnMap);

        final OfficesItem item = (OfficesItem) getChild(groupPosition, childPosition);
        if (langType.equalsIgnoreCase("1")) {
            office_title.setText(item.name_np);
            office_address.setText(item.address_np);
            office_phone.setText(item.phone_np);
            office_fax.setText(item.fax_np);
            btnFile.setText("फाइल हेर्नुहोस्");
            btnMap.setText("नक्सा हेर्नुहोस्");
        } else if (langType.equalsIgnoreCase("2")) {
            office_title.setText(item.name_en);
            office_address.setText(item.address_en);
            office_phone.setText(item.phone_en);
            office_fax.setText(item.fax_en);
        }
        String image_url = "http://202.45.144.52/office/";

        image_url = image_url + item.image;
        Picasso.with(mContext).load(image_url)
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .error(R.drawable.citizen)
                .into(imageView);


        btnFile.setOnClickListener(new View.OnClickListener() {
            String file_url = "http://202.45.144.52/office/" + item.file;

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(file_url));
                mContext.startActivity(intent);
            }
        });
        btnMap.setOnClickListener(new View.OnClickListener() {
            String map_url = item.map_url;

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(map_url));
                mContext.startActivity(intent);
            }
        });
        office_phone.setOnClickListener(new View.OnClickListener() {
            String map_url = item.map_url;

            @Override
            public void onClick(View v) {
                String number = "tel:" + item.phone_en.trim();
                Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mContext.startActivity(callIntent);
            }
        });

        return v;
    }

    private class DistrictItem {
        final String district_name_en, district_name_np;
        final int district_id;

        public DistrictItem(String district_name_en, String district_name_np, int district_id) {
            this.district_name_en = district_name_en;
            this.district_name_np = district_name_np;
            this.district_id = district_id;
        }
    }

    private class OfficesItem {
        final String name_en;
        final String name_np;
        final String address_en;
        final String address_np;
        final String phone_en;
        final String phone_np;
        final String fax_en;
        final String fax_np;
        final String district_name_en;
        final String district_name_np;
        final String file;
        final String image;
        final String map_url;
        final String Id;

        public OfficesItem(String name_en, String name_np, String address_en, String address_np, String phone_en, String phone_np, String fax_en, String fax_np, String district_name_en, String district_name_np, String file, String image, String map_url, String id) {
            this.name_en = name_en;
            this.name_np = name_np;
            this.address_en = address_en;
            this.address_np = address_np;
            this.phone_en = phone_en;
            this.phone_np = phone_np;
            this.fax_en = fax_en;
            this.fax_np = fax_np;
            this.district_name_en = district_name_en;
            this.district_name_np = district_name_np;
            this.file = file;
            this.map_url = map_url;
            this.image = image;
            Id = id;
        }
    }
}