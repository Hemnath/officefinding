package com.government.doit.offices;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {

    private static final String TAG = "GCMIntentService";

    //private Controller aController = null;
    com.government.doit.offices.Controller aController = new Controller();

    public GCMIntentService() {
        // Call extended class Constructor GCMBaseIntentService
        super(Config.GOOGLE_SENDER_ID);
    }

    /**
     * Method called on device registered
     **/
    @Override protected void onRegistered(Context context, String registrationId) {

        //Get Global Controller Class object (see application tag in AndroidManifest.xml)
        // if (aController == null) aController = (Controller) getApplicationContext();

        Log.i(TAG, "Device registered: regId = " + registrationId);
        //aController.displayMessageOnScreen(context, "Your device registred with GCM");
        //Log.d("NAME", MainActivity.name);

        //original
        //aController.register(context, MainActivity.name, MainActivity.email, registrationId);

        aController.register(context, registrationId);
    }

    /**
     * Method called on device unregistred
     *
     */
    @Override protected void onUnregistered(Context context, String registrationId) {
        if (aController == null) aController = (Controller) getApplicationContext();
        Log.i(TAG, "Device unregistered");
        aController.displayMessageOnScreen(context, getString(R.string.gcm_unregistered));
        aController.unregister(context, registrationId);
    }

    /**
     * Method called on Receiving a new message from GCM server
     */
    @Override protected void onMessage(Context context, Intent intent) {

        if (aController == null) aController = (Controller) getApplicationContext();

        Log.i(TAG, "Received message");
        String message = intent.getExtras().getString("price");

        aController.displayMessageOnScreen(context, message);
        // notifies user
        generateNotification(context, message);
    }

    /**
     * Method called on receiving a deleted message
     */
    @Override protected void onDeletedMessages(Context context, int total) {

        if (aController == null) aController = (Controller) getApplicationContext();

        Log.i(TAG, "Received deleted messages notification");
        String message = getString(R.string.gcm_deleted, total);
        aController.displayMessageOnScreen(context, message);
        // notifies user
        generateNotification(context, message);
    }

    /**
     * Method called on Error
     */
    @Override public void onError(Context context, String errorId) {

        if (aController == null) aController = (Controller) getApplicationContext();

        Log.i(TAG, "Received error: " + errorId);
        aController.displayMessageOnScreen(context, getString(R.string.gcm_error, errorId));
    }

    @Override protected boolean onRecoverableError(Context context, String errorId) {

        if (aController == null) aController = (Controller) getApplicationContext();

        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        aController.displayMessageOnScreen(context, getString(R.string.gcm_recoverable_error, errorId));
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Create a notification to inform the user that server has sent a message.
     */
    private void generateNotification(Context context, String message) {
        //Latest >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

//        int icon = R.drawable.ic_launcher;
//        long when = System.currentTimeMillis();
//        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        AllNotification notification = new AllNotification(icon, message, when);
//
//        String title = context.getString(R.string.app_name);
//
//        Intent notificationIntent = new Intent(context, MainActivity.class);
//        // set intent so it does not start a new activity
//        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
//        System.out.println("reached here............ ");
//        //notification.setLatestEventInfo(context, title, message, intent);
//        notification.flags |= AllNotification.FLAG_AUTO_CANCEL;
//
//        // Play default notification sound
//        notification.defaults |= AllNotification.DEFAULT_SOUND;
//
//       // notification.sound = Uri.parse("android.resource://" +
//        // context.getPackageName() + "your_sound_file_name.mp3");
//
//        // Vibrate if vibrate is enabled
//        notification.defaults |= AllNotification.DEFAULT_VIBRATE;
//        notificationManager.notify(0, notification);

        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


//        int icon = R.drawable.ic_launcher;
//        long when = System.currentTimeMillis();
//
//        Notification mNotification = new Notification(pas.manish.uniglobecollege.R.drawable.ic_launcher, "notification!!",System.currentTimeMillis());
//        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

       // Intent intent = new android.content.Intent("pas.manish.uniglobecollege.AllNotification");
        //PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

//        android.net.Uri uri = android.media.RingtoneManager.getDefaultUri(android.media.RingtoneManager.TYPE_NOTIFICATION);
//        Notification.Builder builder = new android.app.Notification.Builder(this);
//        builder.setAutoCancel(false);
//        builder.setTicker("exmple");
//        builder.setContentTitle("Global College of Management");
//        builder.setContentText(message);
//       // builder.setSmallIcon(R.drawable.smallIcon);
//        builder.setContentIntent(pendingIntent);
//        builder.setOngoing(true);
//        builder.setNumber(100);
//        builder.setSound(uri);
//        builder.setVibrate(new long[]{1000, 1000, 1000, 1000});
//
//        mNotification = builder.getNotification();
//        notificationManager.notify(0, mNotification);



        Intent notificationIntent = new Intent(GCMIntentService.this,com.government.doit.offices.MainActivity.class);
        //notificationIntent.setData(android.net.Uri.parse("www.google.com"));
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification = new android.support.v4.app.NotificationCompat.Builder(this)
                .setCategory(Notification.CATEGORY_ALARM)
                .setContentTitle("Uniglobe HSS")
                .setContentText(message)
                .setSmallIcon(com.government.doit.offices.R.drawable.small_notification)
                .setAutoCancel(true)
                .addAction(0, null, contentIntent)
                .setContentIntent(contentIntent)
                .setPriority(Notification.PRIORITY_HIGH)
                .setVibrate(new long[]{1000,1000,1000,1000}).build();

//.addAction(android.R.drawable.ic_menu_view, "View Details", contentIntent)
                NotificationManager notificationManager1  = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager1.notify(0,notification);



    }

}
