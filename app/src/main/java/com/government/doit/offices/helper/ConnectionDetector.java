package com.government.doit.offices.helper;

public class ConnectionDetector {
 
    private android.content.Context _context;
 
    public ConnectionDetector(android.content.Context context){
        this._context = context;
    }
    public boolean isConnectedToInternet(){
        android.net.ConnectivityManager connectivity = (android.net.ConnectivityManager) _context.getSystemService(android.content.Context.CONNECTIVITY_SERVICE);
          if (connectivity != null)
          {
              android.net.NetworkInfo[] info = connectivity.getAllNetworkInfo();
              if (info != null)
                  for (int i = 0; i < info.length; i++)
                      if (info[i].getState() == android.net.NetworkInfo.State.CONNECTED)
                      {
                          return true;
                      }
 
          }
          return false;
    }
}
