package com.government.doit.offices;

public interface Config {
    String YOUR_SERVER_URL = "http://202.166.207.142/demo/global/api/app/device_id/";
    String GOOGLE_SENDER_ID = "639961664778";
    String TAG = "Notification";

    String DISPLAY_MESSAGE_ACTION = "com.government.doit.offices.DISPLAY_MESSAGE";
    String EXTRA_MESSAGE = "message";
}
