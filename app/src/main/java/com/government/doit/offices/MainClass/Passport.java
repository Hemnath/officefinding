package com.government.doit.offices.MainClass;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ExpandableListView;

import com.government.doit.offices.R;
import com.government.doit.offices.helper.AlertDialogManager;
import com.government.doit.offices.helper.CitizenOfficeExpListAdapter;
import com.government.doit.offices.helper.ConnectionDetector;
import com.government.doit.offices.helper.JSONfunctions;
import com.government.doit.offices.helper.PassportExpAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

public class Passport extends AppCompatActivity implements SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {

    JSONObject jsonobject;
    JSONArray jsonarray;

    AlertDialogManager alert = new AlertDialogManager();
    ConnectionDetector cd;
    private ExpandableListView officesListview;
    private PassportExpAdapter officesListAdapter;

    int lastExpandedPosition = -1;

    public static final String OFFICEPREFERENCES = "prefs";
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor edit;
    public static final String strLangType = "LangType";

    private String langType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passport);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(OFFICEPREFERENCES, Context.MODE_PRIVATE);
        edit = sharedpreferences.edit();
        if (sharedpreferences.contains(strLangType))
            langType = sharedpreferences.getString(strLangType, "1");
        else
            langType = "1";

        if (langType.equalsIgnoreCase("1"))
            getSupportActionBar().setTitle(getString(R.string.app_name_np));
        else if (langType.equalsIgnoreCase("2"))
            getSupportActionBar().setTitle(getString(R.string.app_name));


        officesListview = (ExpandableListView) findViewById(R.id.officesList);
        officesListAdapter = new PassportExpAdapter(this);
        officesListview.setAdapter(officesListAdapter);

        cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectedToInternet()) {
            if (langType.equalsIgnoreCase("1"))
                alert.showAlertDialog(Passport.this, "इन्टरनेट जडानमा त्रुटी !", "कृपया यो उपकरणलाई इन्टरनेट संग जोडनुहोस", false);
            else if (langType.equalsIgnoreCase("2"))
                alert.showAlertDialog(Passport.this, "Internet Connection Error", "Please connect to working Internet connection", false);
            return;
        }

        new DownloadJSON().execute();


        officesListview.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    officesListview.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

    }

    public String getLangType() {
        return langType;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search_items, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(this, Citizenship.class)));
        searchView.setIconifiedByDefault(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onClose() {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        officesListAdapter.filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        officesListAdapter.filter(newText);
        return false;
    }

    private class DownloadJSON extends AsyncTask<Object, Object, JSONObject> {
        @Override
        protected JSONObject doInBackground(Object... params) {
            jsonobject = JSONfunctions.getJSONfromURL("http://202.45.144.52/office/api/application/passport_data");
            return jsonobject;
        }

        @Override
        protected void onPostExecute(JSONObject args) {
            try {
                jsonarray = jsonobject.getJSONArray("passport_data");
                for (int i = 0; i < jsonarray.length(); i++) {
                    JSONObject jsonObject = jsonarray.getJSONObject(i);

                    String office_id = jsonObject.getString("id");
                    String name_en = jsonObject.getString("office_name_en");
                    String name_np = jsonObject.getString("office_name_np");
                    String address_en = jsonObject.getString("office_address_en");
                    String address_np = jsonObject.getString("office_address_np");
                    String phone_en = jsonObject.getString("office_phone_en");
                    String phone_np = jsonObject.getString("office_phone_np");
                    String fax_en = jsonObject.getString("office_fax_en");
                    String fax_np = jsonObject.getString("office_fax_np");
                    String map_url = jsonObject.getString("map_url");
                    String image = jsonObject.getString("image");
                    String file = jsonObject.getString("file");

                    String district_name_en = jsonObject.getString("district_name_en");
                    String district_name_np = jsonObject.getString("district_name_np");

                    officesListAdapter.setExpandableList(district_name_en, district_name_np, Integer.parseInt(office_id), name_en, name_np, address_en, address_np, phone_en, phone_np, fax_en, fax_np, file, image, map_url);
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            officesListAdapter.notifyDataSetChanged();
        }
    }
}
