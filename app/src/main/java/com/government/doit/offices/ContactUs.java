package com.government.doit.offices;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ContactUs extends AppCompatActivity {

    String mapUrl = "https://www.google.com/maps/place/27%C2%B041'32.3%22N+85%C2%B019'53.3%22E/@27.6923194,85.3308105,309m/data=!3m2!1e3!4b1!4m5!3m4!1s0x0:0x0!8m2!3d27.692318!4d85.331465?hl=en-US";

    public static final String OFFICEPREFERENCES = "prefs";
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor edit;
    public static final String strLangType = "LangType";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_contact);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        android.webkit.WebView webView = (android.webkit.WebView) findViewById(com.government.doit.offices.R.id.wvMap);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.setWebViewClient(new android.webkit.WebViewClient());
        webView.loadUrl(mapUrl);

        sharedpreferences = getSharedPreferences(OFFICEPREFERENCES, Context.MODE_PRIVATE);
        edit = sharedpreferences.edit();
        String langType;
        if (sharedpreferences.contains(strLangType))
            langType = sharedpreferences.getString(strLangType, "1");
        else
            langType = "1";


        Button button_phone1 = (Button) findViewById(R.id.btnPhone1);
        Button button_phone2 = (Button) findViewById(R.id.btnPhone2);
        Button btnEmail = (Button) findViewById(R.id.btnEmail);
        TextView textView1 = (TextView) findViewById(R.id.textView1);
        TextView textView2 = (TextView) findViewById(R.id.textView2);

        if (langType.equalsIgnoreCase("1")) {
            button_phone2.setText(" १४४९१५९८");
            button_phone1.setText(" १४४९१४३९");
            btnEmail.setText("इमेल : info@doit.gov.np");
            textView1.setText("थापागाउँ ,नया बानेश्वर ");
            textView2.setText("काठमाडौँ ,नेपाल");
        }


        button_phone1.setOnClickListener(
                new android.view.View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent callIntent = new android.content.Intent(Intent.ACTION_CALL);
                        callIntent.setData(android.net.Uri.parse("tel:014491439"));
                        if (ActivityCompat.checkSelfPermission(ContactUs.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            return;
                        }
                        startActivity(callIntent);
                    }
                }

        );
        button_phone2.setOnClickListener(
                new android.view.View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent callIntent = new android.content.Intent(Intent.ACTION_CALL);
                        callIntent.setData(android.net.Uri.parse("tel:014491598"));
                        if (ActivityCompat.checkSelfPermission(ContactUs.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling

                            return;
                        }
                        startActivity(callIntent);
                    }
                }

        );
    }
}
